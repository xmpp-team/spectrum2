Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Spectrum2
Upstream-Contact: Spectrum.IM Transports Team
Source: https://github.com/SpectrumIM/spectrum2
Files-Excluded: backends/frotz/*
 spectrum_manager/src/html/bootstrap.min.css
 spectrum_manager/src/html/js/bootbox.min.js
 spectrum_manager/src/html/js/bootstrap.min.js
 spectrum_manager/src/html/js/jquery-ui.js
 spectrum_manager/src/html/js/jquery.cookie.js
 spectrum_manager/src/html/js/jquery.js
 spectrum_manager/src/server/*
 packaging/debian
 packaging/docker
 packaging/fedora
 .github/*
 .gitignore
 .dockerignore
Comment:
 - Removing frotz as its copyright is unclear, a old version had no grant on modification.
 - spectrum2 webui uses incompatible licenses, therefore removed. (mongoose is GPL2, incompatible with spectrums' GPL3)

Files: *
Copyright: 2009-2019, Jan Kaluza <hanzz.k@gmail.com>
           2009 mathias ertl
           1997, 1998 alembic petrofsky <alembic@petrofsky.berkeley.ca.us>
           1995-1997 stefan jokisch
License: GPL-2+

Files: backends/twitter/libtwitcurl/*
Copyright: 2011 by swatkat (swatkat.thinkdigitATgmailDOTcom)
License: MIT


Files: backends/twitter/libtwitcurl/base64.cpp
       backends/twitter/libtwitcurl/base64.h
Copyright: copyright (c) 2004-2008 René Nyffenegger
License: Zlib


Files: backends/twitter/libtwitcurl/SHA1.*
       backends/twitter/libtwitcurl/HMAC_SHA1.*
Copyright: Dominik Reichl <dominik.reichl@t-online.de>
           Chien-Chung, Chung (Jim Chung) <jimchung1221@gmail.com>
License: Public-domain-ref
 100% free public domain implementation of the SHA-1 algorithm


Files: include/transport/utf8.h
       include/transport/utf8/unchecked.h
       include/transport/utf8/core.h
       include/transport/utf8/checked.h
       cmake_modules/FindJsonCpp.cmake
Copyright: 2006 Nemanja Trifunovic
           2011, Philippe Crassous
           2016, Sensics, Inc.
License: BSL-1.0


Files: include/Swiften/TLS/Schannel/SchannelServerContextFactory.h
       include/Swiften/TLS/Schannel/SchannelServerContextFactory.cpp
       include/Swiften/TLS/Schannel/SchannelServerContext.h
       include/Swiften/TLS/Schannel/SchannelServerContext.cpp
       include/Swiften/Serializer/PayloadSerializers/XHTMLIMSerializer.h
       include/Swiften/Serializer/PayloadSerializers/XHTMLIMSerializer.cpp
       include/Swiften/Serializer/PayloadSerializers/StatsSerializer.h
       include/Swiften/Serializer/PayloadSerializers/StatsSerializer.cpp
       include/Swiften/Serializer/PayloadSerializers/SpectrumErrorSerializer.h
       include/Swiften/Serializer/PayloadSerializers/SpectrumErrorSerializer.cpp
       include/Swiften/Serializer/PayloadSerializers/PrivilegeSerializer.h
       include/Swiften/Serializer/PayloadSerializers/PrivilegeSerializer.cpp
       include/Swiften/Serializer/PayloadSerializers/InvisibleSerializer.h
       include/Swiften/Serializer/PayloadSerializers/InvisibleSerializer.cpp
       include/Swiften/Serializer/PayloadSerializers/HintPayloadSerializer.h
       include/Swiften/Serializer/PayloadSerializers/HintPayloadSerializer.cpp
       include/Swiften/Serializer/PayloadSerializers/GatewayPayloadSerializer.h
       include/Swiften/Serializer/PayloadSerializers/GatewayPayloadSerializer.cpp
       include/Swiften/Serializer/PayloadSerializers/AttentionSerializer.h
       include/Swiften/Serializer/PayloadSerializers/AttentionSerializer.cpp
       include/Swiften/Parser/StringTreeParser.h
       include/Swiften/Parser/StringTreeParser.cpp
       include/Swiften/Parser/PayloadParsers/XHTMLIMParser.h
       include/Swiften/Parser/PayloadParsers/XHTMLIMParser.cpp
       include/Swiften/Parser/PayloadParsers/StatsParser.h
       include/Swiften/Parser/PayloadParsers/StatsParser.cpp
       include/Swiften/Parser/PayloadParsers/PrivilegeParser.h
       include/Swiften/Parser/PayloadParsers/PrivilegeParser.cpp
       include/Swiften/Parser/PayloadParsers/InvisibleParser.h
       include/Swiften/Parser/PayloadParsers/InvisibleParser.cpp
       include/Swiften/Parser/PayloadParsers/HintPayloadParser.h
       include/Swiften/Parser/PayloadParsers/HintPayloadParser.cpp
       include/Swiften/Parser/PayloadParsers/GatewayPayloadParser.h
       include/Swiften/Parser/PayloadParsers/GatewayPayloadParser.cpp
       include/Swiften/Parser/PayloadParsers/AttentionParser.h
       include/Swiften/Parser/PayloadParsers/AttentionParser.cpp
       include/Swiften/Network/DummyConnectionServerFactory.h
       include/Swiften/Network/DummyConnectionServerFactory.cpp
       include/Swiften/Elements/XHTMLIMPayload.h
       include/Swiften/Elements/XHTMLIMPayload.cpp
       include/Swiften/Elements/StatsPayload.h
       include/Swiften/Elements/StatsPayload.cpp
       include/Swiften/Elements/SpectrumErrorPayload.h
       include/Swiften/Elements/SpectrumErrorPayload.cpp
       include/Swiften/Elements/Privilege.h
       include/Swiften/Elements/Privilege.cpp
       include/Swiften/Elements/InvisiblePayload.h
       include/Swiften/Elements/InvisiblePayload.cpp
       include/Swiften/Elements/HintPayload.h
       include/Swiften/Elements/HintPayload.cpp
       include/Swiften/Elements/GatewayPayload.h
       include/Swiften/Elements/GatewayPayload.cpp
       include/Swiften/Elements/AttentionPayload.h
       include/Swiften/Elements/AttentionPayload.cpp
Copyright: 2011 Soren Dreijer
           2011 Jan Kaluza
Comment: Originates from project swiften-im, https://swift.im/
License: BSD-2-Clause


Files: include/Swiften/TLS/OpenSSL/OpenSSLServerContextFactory.h
       include/Swiften/TLS/OpenSSL/OpenSSLServerContextFactory.cpp
       include/Swiften/TLS/OpenSSL/OpenSSLServerContext.h
       include/Swiften/TLS/OpenSSL/OpenSSLServerContext.cpp
       include/Swiften/TLS/SecureTransport/SecureTransportServerContext.mm
       include/Swiften/TLS/SecureTransport/SecureTransportServerContext.h
       include/Swiften/TLS/TLSServerContextFactory.h
       include/Swiften/TLS/TLSServerContextFactory.cpp
       include/Swiften/TLS/TLSServerContext.h
       include/Swiften/TLS/TLSServerContext.cpp
       include/Swiften/StreamStack/TLSServerLayer.h
       include/Swiften/StreamStack/TLSServerLayer.cpp
       include/Swiften/Server/UserRegistry.h
       include/Swiften/Server/UserRegistry.cpp
       include/Swiften/Server/SimpleUserRegistry.h
       include/Swiften/Server/SimpleUserRegistry.cpp
       include/Swiften/Server/ServerStanzaRouter.h
       include/Swiften/Server/ServerStanzaRouter.cpp
       include/Swiften/Server/ServerStanzaChannel.h
       include/Swiften/Server/ServerStanzaChannel.cpp
       include/Swiften/Server/ServerSession.h
       include/Swiften/Server/ServerSession.cpp
       include/Swiften/Server/ServerFromClientSession.h
       include/Swiften/Server/ServerFromClientSession.cpp
       include/Swiften/Server/Server.h
       include/Swiften/Server/Server.cpp
       include/Swiften/Parser/PayloadParsers/MUCPayloadParser.h
       include/Swiften/Parser/PayloadParsers/MUCPayloadParser.cpp
       include/Swiften/Network/DummyNetworkFactories.h
       include/Swiften/Network/DummyNetworkFactories.cpp
       include/Swiften/Network/DummyConnectionServer.h
       include/Swiften/Network/DummyConnectionServer.cpp
Copyright: 2010 Remko Tronçon
           2010 Kevin Smith
Comment: Originates from project swiften-im, https://swift.im/
License: GPL-3


Files: cmake_modules/Findopenssl.cmake
       cmake_modules/FindPROTOBUF.cmake
       cmake_modules/FindBoost.cmake
Copyright: 2006-2009 Kitware, Inc.
           2006-2008 Andreas Schneider <mail@cynapses.org>
           2006 Alexander Neundorf <neundorf@kde.org>
           2007 Wengo
           2007 Mike Jackson
           2008 Andreas Pakulat <apaku@gmx.de>
           2008-2009 Philip Lowman <philip@yhbt.com>
           2008 Esben Mose Hansen, Ange Optimization ApS
           2009-2011 Mathieu Malaterre <mathieu.malaterre@gmail.com>
Comment: Origin is the CMake project, licensed as BSD-3
License: BSD-3-clause


Files: debian/*
Copyright: 2010-2012, Mathias Ertl <apt-repository@fsinf.at>
           2019, Dominik George <natureshadow@debian.org>
           2022-2024, Tobias Frost <tobi@debian.org>
License: GPL-2+


License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2.  Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 * Neither the names of Kitware, Inc., the Insight Software Consortium,
   nor the names of their contributors may be used to endorse or promote
   products derived from this software without specific prior written
   permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


License: BSL-1.0
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.


License: GPL-2
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 Version 2 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 '/usr/share/common-licenses/GPL-2'.


License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.


License: GPL-3
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 Version 3 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 '/usr/share/common-licenses/GPL-3'.


License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: Zlib
 This source code is provided 'as-is', without any express or implied
 warranty. In no event will the author be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this source code must not be misrepresented; you must not
    claim that you wrote the original source code. If you use this source code
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original source code.
 .
 3. This notice may not be removed or altered from any source distribution.
